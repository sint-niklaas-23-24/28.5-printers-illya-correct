﻿namespace _28._5___Printers
{
    internal class Printer
    {
        private bool _bezig;
        private string _naam;
        public Printer(string naam)
        {
            Naam = naam;
        }
        public bool Bezig { get { return _bezig; } set { _bezig = value; } }
        public string Naam { get { return _naam; } set { _naam = value; } }
        public override string ToString()
        {
            string printerStatus;
            if (Bezig)
            {
                printerStatus = Naam + ": " + "Bezig met een privé-opdracht.";
            }
            else
            {
                printerStatus = Naam + ": " + "Wachtend op een privé-opdracht.";

            }
            return printerStatus;
        }
        public void Reset()
        {
            Bezig = false;
        }
    }
}

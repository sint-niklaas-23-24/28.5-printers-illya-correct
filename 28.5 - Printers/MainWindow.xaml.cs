﻿using System.Windows;
using System.Windows.Media.Imaging;

namespace _28._5___Printers
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        PC Desktop;
        Printer printerEen;
        Printer printerTwee;
        Printer printerDrie;
        Printer printerVier;
        int teller;

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            Desktop.PrintAf();
            RefreshLabelsEnImages();
        }

        private void RefreshLabelsEnImages()
        {
            if (printerEen.Bezig == true)
            {
                imgPrinter1.Source = new BitmapImage(new Uri("Images/printer_busy.gif", UriKind.Relative));
                lblPrinter1.Content = printerEen.ToString();
            }
            if (printerTwee.Bezig == true)
            {
                imgPrinter2.Source = new BitmapImage(new Uri("Images/printer_busy.gif", UriKind.Relative));
                lblPrinter2.Content = printerTwee.ToString();

            }
            if (printerDrie.Bezig == true)
            {
                imgPrinter3.Source = new BitmapImage(new Uri("Images/printer_busy.gif", UriKind.Relative));
                lblPrinter3.Content = printerDrie.ToString();
            }
            if (printerVier.Bezig == true)
            {
                imgPrinter4.Source = new BitmapImage(new Uri("Images/printer_busy.gif", UriKind.Relative));
                lblPrinter4.Content = printerVier.ToString();

            }
        }

        private void btnPrinter1_Click(object sender, RoutedEventArgs e)
        {
            printerEen.Bezig = false;
            imgPrinter1.Source = new BitmapImage(new Uri("Images/printer_notbusy.jpg", UriKind.Relative));
            lblPrinter1.Content = printerEen.ToString();
        }

        private void btnPrinter2_Click(object sender, RoutedEventArgs e)
        {
            printerTwee.Bezig = false;
            imgPrinter2.Source = new BitmapImage(new Uri("Images/printer_notbusy.jpg", UriKind.Relative));
            lblPrinter2.Content = printerTwee.ToString();
        }

        private void btnPrinter3_Click(object sender, RoutedEventArgs e)
        {
            printerDrie.Bezig = false;
            imgPrinter3.Source = new BitmapImage(new Uri("Images/printer_notbusy.jpg", UriKind.Relative));
            lblPrinter3.Content = printerDrie.ToString();
        }

        private void btnPrinter4_Click(object sender, RoutedEventArgs e)
        {
            printerVier.Bezig = false;
            imgPrinter4.Source = new BitmapImage(new Uri("Images/printer_notbusy.jpg", UriKind.Relative));
            lblPrinter4.Content = printerVier.ToString();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Desktop = new PC();
            printerEen = new Printer("Printer 1");
            printerTwee = new Printer("Printer 2");
            printerDrie = new Printer("Printer 3");
            printerVier = new Printer("Printer 4");
            Desktop.AddPrinter(printerEen);
            Desktop.AddPrinter(printerTwee);
            Desktop.AddPrinter(printerDrie);
            Desktop.AddPrinter(printerVier);
            lblPrinter1.Content = printerEen.ToString();
            lblPrinter2.Content = printerTwee.ToString();
            lblPrinter3.Content = printerDrie.ToString();
            lblPrinter4.Content = printerVier.ToString();


        }
    }
}
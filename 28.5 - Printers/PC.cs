﻿using System.Windows;

namespace _28._5___Printers
{
    internal class PC
    {
        private List<Printer> _printers;
        public PC()
        {
            Printers = new List<Printer>();
        }
        public List<Printer> Printers { get { return _printers; } set { _printers = value; } }
        public void AddPrinter(Printer p)
        {
            Printers.Add(p);
        }
        public bool PrintAf()
        {
            bool Printerbeschikbaar = false;
            foreach (Printer printer in Printers)
            {
                if (printer.Bezig == false)
                {
                    printer.Bezig = true;
                    Printerbeschikbaar = true;
                    break;
                }
            }
            if (Printerbeschikbaar == false)
            {
                MessageBox.Show("Er zijn geen printers beschikbaar om deze opdracht uit te voeren", "Geen printers beschikbaar", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return Printerbeschikbaar;


        }

    }

}
